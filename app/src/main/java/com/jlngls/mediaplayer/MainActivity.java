package com.jlngls.mediaplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;
    private SeekBar seekBarVolum;
    private AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );


        mediaPlayer = MediaPlayer.create( getApplicationContext(), R.raw.teste );
        inicializarSeekBar();
    }


    public void inicializarSeekBar() {

        seekBarVolum = findViewById( R.id.seekBarID );

        //configurar AUDIO MANAGER
        audioManager = (AudioManager) getSystemService( Context.AUDIO_SERVICE );

        // recuperar valores do volume
        int volumeMaximo = audioManager.getStreamMaxVolume( AudioManager.STREAM_MUSIC );


        seekBarVolum.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                audioManager.setStreamVolume( AudioManager.STREAM_MUSIC, i, AudioManager.FLAG_SHOW_UI );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        } );

    }

    public void tocarMusica(View view) {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }

    }

    public void pausarMusica(View view) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }

    public void pararMusica(View view) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer = MediaPlayer.create( getApplicationContext(), R.raw.teste );
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release(); // liberar recursos de midia que estao execuntando com a classe mediaplayer
            mediaPlayer = null;
        }

    }


}